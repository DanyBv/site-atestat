<?php
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        if(!isset($_POST['username']) || !isset($_POST['password']) || !isset($_POST['confirmPassword']) || !isset($_POST['email']) || strlen($_POST['username']) == 0 || strlen($_POST['password']) == 0 || strlen($_POST['confirmPassword']) == 0 || strlen($_POST['email']) == 0)
            echo 'Nu ai completat toate campurile.';
        else
        {
            echo 'Vad ca incerci sa te inregistrezi, ' . $_POST['username'] . '.';
        }
    }
?>
<form action="/?page=register" method="post">
    <div>
        Nume de utilizator:
        <input type="text" name="username">
    </div>
    <div>
        Parola:
        <input type="password" name="password">
    </div>
    <div>
        Confirmă parola:
        <input type="password" name="confirmPassword">
    </div>
    <div>
        Email:
        <input type="email" name="email">
    </div>
    <div>
        <input type="submit" value="Înregistrare">
        <input type="reset" value="Resetare">
    </div>
</form>