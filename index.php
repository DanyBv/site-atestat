<?php
    include('include/config.php');
    include('include/database.php');
    include('include/functions.php');
    $pageTitle = "Acasă"   ;
    $pageFile  = "home.php";
    if(isset($_GET['page']))
    {
        $page = $_GET['page'];
        if($page == 'register') {
            $pageTitle = "Înregistrare";
            $pageFile = "register.php";
        } else if($page == 'login') {
            $pageTitle = "Conectare";
            $pageFile = "login.php";
        } else if($page != 'home') {
            $pageTitle = "404";
            $pageFile = "404.php";
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $siteTitle; ?> - <?php echo $pageTitle; ?></title>
    </head>
    <body>
        <?php
            include('pages/' . $pageFile);
        ?>
    </body>
</html>